#include <WiFi.h>
#include <PubSubClient.h>
#include <PZEM004Tv30.h>

const char* ssid = "M2_Core";
const char* password =  "khoithuekhackleo";
const char* mqttServer = "mqtt.mounoydev.com";
const int mqttPort = 1883;
const char* mqttUser = "mn";
const char* mqttPassword = "mn";
String messageVoltage;

String msgStr = "";

#if defined(ESP32)
// PZEM004Tv30 pzem(Serial2, 16, 17);
PZEM004Tv30 pzem(Serial2, 15, 2);
#else
PZEM004Tv30 pzem(Serial2);
#endif

WiFiClient espClient;
PubSubClient client(espClient);
 
void setup() {
 
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
 
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP32Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }

  pzem.resetEnergy();

  float voltage = pzem.voltage();
  float current = pzem.current();
  float power = pzem.power();
  float energy = pzem.energy();
  float frequency = pzem.frequency();
  float pf = pzem.pf();

  msgStr = "{\"voltage\":" + String(voltage) + ",\"current\":" + String(current) + \"power\":" + String(power) + ",\"frequency\":" + String(frequency)" + ",\"pf\":" + String(pf)"}";
  byte arrSize = msgStr.length() + 1; //set size of array
  char msg[arrSize];
  
  msgStr.toCharArray(msg, arrSize);
  client.publish("evapp", msg); //publish
 
}
 
void loop() {
  Serial.print("Custom Address:");
    Serial.println(pzem.readAddress(), HEX);

    // Read the data from the sensor
    float voltage = pzem.voltage();
    float current = pzem.current();
    float power = pzem.power();
    float energy = pzem.energy();
    float frequency = pzem.frequency();
    float pf = pzem.pf();

    // Check if the data is valid
    if(isnan(voltage)){
        Serial.println("Error reading voltage");
    } else if (isnan(current)) {
        Serial.println("Error reading current");
    } else if (isnan(power)) {
        Serial.println("Error reading power");
    } else if (isnan(energy)) {
        Serial.println("Error reading energy");
    } else if (isnan(frequency)) {
        Serial.println("Error reading frequency");
    } else if (isnan(pf)) {
        Serial.println("Error reading power factor");
    } else {

        // Print the values to the Serial console
        Serial.print("Voltage: ");      Serial.print(voltage);      Serial.println("V");
        Serial.print("Current: ");      Serial.print(current);      Serial.println("A");
        Serial.print("Power: ");        Serial.print(power);        Serial.println("W");
        Serial.print("Energy: ");       Serial.print(energy,3);     Serial.println("kWh");
        Serial.print("Frequency: ");    Serial.print(frequency, 1); Serial.println("Hz");
        Serial.print("PF: ");           Serial.println(pf);

    }

    Serial.println();
    delay(2000);
}
