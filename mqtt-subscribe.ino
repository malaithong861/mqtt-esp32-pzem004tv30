#include <WiFi.h>
#include <PubSubClient.h>
 
const char* ssid = "M2_Core";
const char* password =  "khoithuekhackleo";
const char* mqttServer = "mqtt.mounoydev.com";
const int mqttPort = 1883;
const char* mqttUser = "mn";
const char* mqttPassword = "mn";
 
WiFiClient espClient;
PubSubClient client(espClient);
const int output5 = 5;
 
void callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  String messageTemp;
  Serial.print("Message:");
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    messageTemp += (char)payload[i];
  }
 
  Serial.println();
  Serial.println("-----------------------");

 if (String(topic) == "evapp") {
    Serial.print("Changing output to ");
    if(messageTemp == "on"){
      Serial.println("on");
      digitalWrite(output5, LOW);
    }
    else if(messageTemp == "off"){
      Serial.println("off");
      digitalWrite(output5, HIGH);
    }
  }
}
 
void setup() {
 
  Serial.begin(115200);
  pinMode(output5, OUTPUT);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP32Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }
 
  client.subscribe("evapp");
 
}
 
void loop() {
  client.loop();
  
}
